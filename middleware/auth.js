const servicios = require('../servicios');

function isAuth(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(403).send({mensaje: 'El usuario no esta autorizado a continuar'})
    }
    const token = req.headers.authorization.split(" ")[1];
    servicios.decodeToken(token).then(response=>{
        req.usuario = response;
        next();
    }).catch(response=>{
        res.status(response.status)
    })
    next()
}

module.exports = isAuth;
