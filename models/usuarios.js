const mongoose = require('mongoose');
const Schema = mongoose.Schema
const bcrypt = require('bcrypt-nodejs');

const UsuarioSchema = new Schema({
    correo: {type: String, unique: true, lowercase:true},
    nombre: String,
    contraseña: {type:String, select:false},
    user: {type:String, unique:true, lowercase:true},
    registro: {type:Date, default:Date.now()}
})

UsuarioSchema.pre('save', function(next) {
    let user = this
    if(!user.isModified('contraseña')) return next()
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err)
        bcrypt.hash(this.contraseña, salt, null, (err, hash) => {
            if (err) return next(err)
            this.contraseña = hash
            next()
        })
    })
})

module.exports = mongoose.model('usuarios', UsuarioSchema)
