
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PacientesSchema = Schema({
    nombre: String,
    dni: {type:String, unique: true},
    direccion:String,
    tlf: String,
    medico: String,
    alergias: String,
    tipo_op: {
        type: String,
    },
    tipo_sag: {
        type: String,
        enum:[
            'O+',
            'O-',
            'A+',
            'A-',
            'B+',
            'B-',
            'AB+',
            'AB-'
        ]
    }
})


module.exports = mongoose.model('pacientes', PacientesSchema)
