const express = require('express');
const PacientesController = require('../controllers/pacientes');
const UsuarioCntrl = require('../controllers/usuarios')
const auth = require('../middleware/auth')
const api = express.Router()
const axios = require('axios');

api.get('/pacientes', PacientesController.VerPacientes)
api.get('/paciente/:paciente_id', auth, PacientesController.VerPaciente)
api.post('/paciente', PacientesController.Guardar)
api.put('/paciente/:paciente_id', auth, PacientesController.Actualizar)
api.delete('/paciente/:paciente_id', auth, PacientesController.Borrar)
api.post('/signup', UsuarioCntrl.signUp)
api.post('/signin', UsuarioCntrl.signIn)
api.get('/', (req, res) => {
	let pacientesArray = [];
	axios.get('http://localhost:3001/pacientes')
		.then((response)=>{
			let pacientesArray = response.data.pacients;
			console.log(pacientesArray)
			res.render('index', {
				pacientes:pacientesArray
			});
		})
		.catch((err)=>{
			console.log(err)
		})
})

module.exports = api
