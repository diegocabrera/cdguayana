const config = require('./config')
const mongoose = require('mongoose')
const app = require('./app')


mongoose.connect(process.env.urlDB, (err, res) =>{
    if(err){
        return console.log('No se pudo conectar al servidor de base de datos');
    }
    app.listen(process.env.PORT, ()=>{
        console.log('El servidor esta corriendo en el puerto: ' + config.port);
    })
})
