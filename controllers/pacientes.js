const Paciente = require('../models/pacientes');

function VerPacientes (req, res) {
    //Modelo.find({}) => Buscar todos los registros del modelo
    Paciente.find({}, (err,pacients)=>{
        if (err) {
            return res.status(500).send({mensaje: 'Ha ocurrido un error interno de Servidor: ' + err});
        }
        if (!pacients) {
            return res.status(404).send({mensaje: `El paciente solicitado, no existe`});
        }
        res.status(200).send({pacients});
    })
}

function VerPaciente (req, res) {

    let paciente_id = req.params.paciente_id
    //Modelo.findById(id_a_buscar) => Buscar un id en especifico dentro del modelo
    Paciente.findById(paciente_id, (err, paciente)=>{
        if (err) {
            return res.status(500).send({mensaje: `Ha ocurrido un error interno de Servidor: ${err}`})
        }
        if (!paciente) {
            return res.status(404).send({mensaje: `El paciente solicitado, no existe`})
        }

        res.status(200).send({paciente})
    })
}

function Guardar(req, res) {
    console.log('POST api/producto')

    let paciente = new Paciente()
    paciente.nombre = req.body.nombre
    paciente.dni = req.body.dni
    paciente.direccion = req.body.direccion
    paciente.tlf = req.body.tlf
    paciente.medico = req.body.medico
    paciente.alergias = req.body.alergias
    paciente.tipo_op = req.body.tipo_op
    paciente.tipo_sag = req.body.tipo_sag

    paciente.save((err, pacienteStored) => {
        if (err) res.status(500).send({message: `Error salvando en la base de datos: ${err}`})

        res.status(200).send({paciente: pacienteStored})
    })
}

function Actualizar (req, res) {
    console.log('PUT /api/paciente')
    let id = req.params.paciente_id
    let cuerpo = req.body

    Paciente.findByIdAndUpdate(id, cuerpo, (err, actualizacion) =>{
        if (err) res.status(500).send({mensaje: `Error Actualizando el producto ${err}`})

        res.status(200).send('El producto: ' + paciente_id + ' ha sido actualizado con normalidad')
    })
}

function Borrar (req, res) {
    console.log('DELETE /api/producto')
    let id = req.params.paciente_id

    Paciente.findById(id,(err, paciente)=>{
        if (err) res.status(500).send({mensaje: `Error eliminando el producto ${err}`})

        paciente.remove( err => {
            if (err) res.status(500).send({mensaje: `Error eliminando el producto ${err}`})
            res.status(200).send({mensaje: `El producto con el ID: ${id} ha sido eliminado`})
        })
    })
}


module.exports = {
    VerPacientes,
    VerPaciente,
    Guardar,
    Actualizar,
    Borrar
}
