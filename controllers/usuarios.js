const mongoose = require('mongoose');
const Usuarios = require('../models/usuarios')
const service = require('../servicios')

function signUp (req, res){
    const usuario = new Usuarios({
        correo: req.body.correo,
        nombre: req.body.nombre,
        user: req.body.usuario,
        contraseña: req.body.contraseña
    })
    usuario.save((err)=>{
        if (err) return res.status(500).send({mensaje: 'No se pudo registar el usuario: ' + err})

        return res.status(200).send({token:service.crearToken(usuario)})
    })
}

function signIn (req, res){
    let user = req.body.user
    Usuarios.find({user: user}, (err, usuario) => {
        req.user = usuario
        if(err) return res.status(500).send({mensaje: 'No se pudo loguear el usuario: ' + err})
        if(!usuario) return res.status(404).send({mensaje: 'el usuario' + usuario + ' no existe'})
        res.json({
            mensaje:'El usuario ha iniciado correctamente',
            token:service.crearToken(usuario)
        })
    })
}

module.exports = {
    signUp,
    signIn
}
