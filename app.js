const express = require('express');
const bodyParser = require('body-parser');
const app = express()
const api = require('./routes')

// Ya se lo mostre, gg

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
app.set('view engine', 'pug')
app.use(express.static('static'))
app.use('/', api)

module.exports = app
